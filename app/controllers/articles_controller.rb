# frozen_string_literal: true

# class controller
class ArticlesController < ApplicationController
  def index
    arr = NewsFeeds::ParseBytebytego.new.call
    NewsFeeds::SaveArticles.new(arr).call
    @articles = Article.all
  end
end
