# frozen_string_literal: true

# class controller
class NewsSourcesController < ApplicationController
  def new
    @news_source = NewsSource.new
  end

  def show
    @news_source = NewsSource.find(params[:id])
  end

  def index
    @news_sources_list = NewsSource.all
  end

  def create
    @news_source = NewsSource.new(post_params)
    if @news_source.save
      redirect_to news_sources_path
    else
      get_errors
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @news_source = NewsSource.find(params[:id])
  end

  def update
    @news_source = NewsSource.find(params[:id])
    if @news_source.update(post_params)
      redirect_to news_sources_path
    else
      get_errors
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @news_source = NewsSource.find(params[:id])
    @news_source.destroy

    redirect_to news_sources_path, status: :see_other
  end

  private

  def post_params
    params.require(:news_source).permit(:url, :title, :isActive)
  end

  def errors
    errors = %i[url title].map { |field| @news_source.errors.full_messages_for(field) }.join("\n")
    flash.now[:error] = errors
  end
end
