# frozen_string_literal: true

# class model
class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
end
