# frozen_string_literal: true

# class model
class NewsSource < ApplicationRecord
  validates :url, presence: true, length: { maximum: 50 }
  validates :title, presence: true, length: { maximum: 50 }
end
