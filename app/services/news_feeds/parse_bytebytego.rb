# frozen_string_literal: true

require 'open-uri'

module NewsFeeds
  # class for parse rss
  class ParseBytebytego
    def initialize
      @hash_array = []
    end

    attr_reader :hash_array

    def call
      doc = Nokogiri::XML(URI.open('https://blog.bytebytego.com/feed'))
      doc.xpath('//item').each do |thing|
        @title = thing.at_xpath('title').content
        @url = thing.at_xpath('link').content
        @hash_array.push({ 'title' => @title, 'url' => @url })
      end

      @hash_array
    end
  end
end
