# frozen_string_literal: true

module NewsFeeds
  # class for save articles from rss
  class SaveArticles
    def initialize(articles_data)
      @articles_data = articles_data
    end

    def call
      @articles_data.each do |article|
        exist_item = Article.find_by(url: article['url'])
        if exist_item.nil?
          article_tmp = Article.new(title: article['title'], url: article['url'])
          article_tmp.save
        end
      end
    end
  end
end
