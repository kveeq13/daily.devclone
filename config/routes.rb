# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'articles#index'

  # get '/news_sources/new', to: 'news_sources#new'
  # get '/news_sources/', to: 'news_sources#show', as: 'index'
  # post '/news_sources/new/create', to: 'news_sources#create'
  resources :news_sources
end
