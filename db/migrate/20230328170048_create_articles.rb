# frozen_string_literal: true

# class for migration
class CreateArticles < ActiveRecord::Migration[7.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :url

      t.timestamps
    end
  end
end
