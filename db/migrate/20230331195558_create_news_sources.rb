# frozen_string_literal: true

# class for migration
class CreateNewsSources < ActiveRecord::Migration[7.0]
  def change
    create_table :news_sources do |t|
      t.string :title
      t.string :url
      t.boolean :isActive

      t.timestamps
    end
  end
end
